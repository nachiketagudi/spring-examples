package com.nachiketa.spring.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.nachiketa.spring.service.Mensuration;

@RestController
public class MensurationController {

	@Autowired
	@Qualifier("circle")
	Mensuration circle;
	@Autowired
	@Qualifier("square")
	Mensuration square;
	
	 @RequestMapping(value = "/compute/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	 public @ResponseBody long compute(@PathVariable("id") int id) {
	 System.out.println("Area of circle "+circle.area(7));
	 System.out.println("Perimeter of circle "+circle.perimeter(7));
	 System.out.println("Area of square "+square.area(7));
	 System.out.println("Perimeter of square "+square.perimeter(7));
	   return 0;
	  }
}
