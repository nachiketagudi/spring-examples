package com.nachiketa.spring.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("square")
@Component
public class MensurationSquareImpl implements Mensuration {

	@Override
	public float area(float input) {
		System.out.println("Computing area of square");
		return input*input;
	}

	@Override
	public float perimeter(float input) {
		System.out.println("Computing Perimeter of square");
		return 4*input;
	}

}
