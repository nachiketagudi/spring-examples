package com.nachiketa.spring.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("circle")
@Component
public class MensurationCircleImpl implements Mensuration{

	@Override
	public float area(float input) {
		System.out.println("Computing Area of circle");
		return (float) (3.14*input*input);
	}

	@Override
	public float perimeter(float input) {
		System.out.println("Computing Perimeter of circle");
		return (float) (2*3.14*input);
	} 

}
