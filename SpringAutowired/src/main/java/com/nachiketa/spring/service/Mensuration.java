package com.nachiketa.spring.service;

import org.springframework.stereotype.Component;

@Component
public interface Mensuration {

	public float area(float input);
	
	public float perimeter(float input);
	
	
}
